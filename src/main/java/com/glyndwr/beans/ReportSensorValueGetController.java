package com.glyndwr.beans;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class ReportSensorValueGetController {
	private static final String ok = "ok value: %s";
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	

    @GetMapping("/sensorvalueget")
    @ResponseBody
    public ReportSensorValueGet receiveValue(@RequestParam(name="sensorValue", required=false, defaultValue="0") String sensorValue) {
    	ReportSensorValueGet sv =  new ReportSensorValueGet(String.format(ok, sensorValue));
    	log.info("sensorvalue recieved: "+sv.getSensorValue());
    	return sv;
    }
}
