package com.glyndwr.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DigitalSensorValueController {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(method = RequestMethod.POST, value = "/hallsensor")
	@ResponseBody
	DigitalSensorValueReply receipt(@RequestBody DigitalSensorValue value) {
		DigitalSensorValueReply reply = new DigitalSensorValueReply();
		log.info("Call received as JSON with content: "+String.valueOf(value.getValue()));
		return reply;
	}
}
