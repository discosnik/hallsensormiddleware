package com.glyndwr.beans;

public class DigitalSensorValue {

	private int sensorValue;

	public int getValue() {
		return sensorValue;
	}

	public void setValue(int value) {
		this.sensorValue = value;
	}
	
}
