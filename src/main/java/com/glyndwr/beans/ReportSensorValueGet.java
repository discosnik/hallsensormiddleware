package com.glyndwr.beans;

public class ReportSensorValueGet {

	private String sensorValue;
	
	public ReportSensorValueGet(String sensorValue) {
		super();
		this.sensorValue = sensorValue;
	}

	public String getSensorValue() {
		return sensorValue;
	}

	public void setSensorValue(String sensorValue) {
		this.sensorValue = sensorValue;
	}
	
	
}
