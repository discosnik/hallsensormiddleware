package com.glyndwr.beans;

public class DigitalSensorValueReply {

	private String ok = "ok";

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}
	
	
}
