package com.glyndwr.server.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.glyndwr"})
public class HallSensorMiddlewareApplication {

	public static void main(String[] args) {
		SpringApplication.run(HallSensorMiddlewareApplication.class, args);
	}
}
